@extends('employee.master')

@section('content')

<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <form class="form-inline" method="post" action="#">
                        <div class="input-group input-group-sm">
                          <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search" name="q">
                          <div class="input-group-append">
                            <button style=" border:none; border-top-right-radius: 10%; border-bottom-right-radius: 10%; background-color: #8EC5FC; background-image: linear-gradient(62deg, #8EC5FC 0%, #E0C3FC 100%);" type="submit">
                              <i class="fas fa-search text-white"></i>
                            </button>
                          </div>
                        </div>
                    </form>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-striped">
                        <thead class="text-center">
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Company</th>
                                <th>Email</th>
                                <th>Phone Number</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $index => $datas)
                            <tr>
                                <td>{{++$index}}</td>
                                <td>{{$datas->fisrt_name}} {{$datas->last_name}}</td>
                                <td>{{$datas->getCompanyName ? $datas->getCompanyName->name : 'notfound'}}</td>
                                <td><a href="mailto:{{$datas->email}}">{{$datas->email}}</a></td>
                                <td><a href="cal:{{$datas->phone}}">{{$datas->phone}}</a></td>
                                <td class="text-center"><a href="{{url('/employee/details/'.$datas->id)}}" title="details"><i class="fas fa-info-circle text-info"></i></a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $data->links() }}
                </div>
            </div>
        </div>
    </div>
</section>

@stop