@extends('employee.master')

@section('content')

<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <img src="{{asset('styles/dist/img/profile.png')}}" class="img-circle" width="70%" alt="Avatar">
                        </div>
                        <div class="col-md-9">
                            <h3 class="text-center text-bold">{{$employee->fisrt_name}} {{$employee->last_name}}</h3>
                            <div class="py-6">
                                <div class="lg-12">
                                    <ul>
                                        <table>
                                            <tr>
                                                <th>Company</th>
                                                <th width="10">:</th>
                                                <td>{{ $employee->getCompanyName ? $employee->getCompanyName->name: 'uknown' }}</td>
                                            </tr>
                                            <tr>
                                                <th>Email</th>
                                                <th width="10">:</th>
                                                <td><a href="mailto:{{ $employee->email }}">{{ $employee->email }}</a></td>
                                            </tr>
                                            <tr>
                                                <th>Phone Number</th>
                                                <th width="10">:</th>
                                                <td><a href="cal:{{ $employee->phone }}">{{ $employee->phone }}</a></td>
                                            </tr>
                                            <tr>
                                                <th>Joined</th>
                                                <th width="10">:</th>
                                                <td>{{ $employee->created_at }}</td>
                                            </tr>
                                            <tr>
                                                <th>Created By</th>
                                                <th width="10">:</th>
                                                <td width="150px">{{ $employee->created_at }}</td>
                                                <td class="text-bold">{{ $employee->getCreatedBy ? $employee->getCreatedBy->name: 'uknown' }}</td>
                                            </tr>
                                            <tr>
                                                <th>Updated By</th>
                                                <th width="10">:</th>
                                                <td width="150px">{{ $employee->updated_at }}</td>
                                                <td class="text-bold">{{ $employee->getUpdatedBy ? $employee->getUpdatedBy->name: 'uknown' }}</td>
                                            </tr>
                                            <tr class="py-3">
                                                <th><a href="{{ url('/employee') }}" title="go back" class="btn btn-warning text-white"> <i class="fas fa-arrow-left"></i> </a></th>
                                            </tr>
                                        </table>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop