<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Employee CRM | Dashboard</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{asset('styles/plugins/fontawesome-free/css/all.min.css')}}">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="{{asset('styles/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <link rel="stylesheet" href="{{asset('styles/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('styles/plugins/jqvmap/jqvmap.min.css')}}">
  <link rel="stylesheet" href="{{asset('styles/dist/css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{asset('styles/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <link rel="stylesheet" href="{{asset('styles/plugins/daterangepicker/daterangepicker.css')}}">
  <link rel="stylesheet" href="{{asset('styles/plugins/summernote/summernote-bs4.css')}}">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  @include('employee.components.header')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  @include('employee.components.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <section class="content">
      <div class="container-fluid">
        @yield('content')
        </div>
      </div>
    </section>
  </div>
  <!-- /.content-wrapper -->
  @include('employee.components.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<script src="{{asset('styles/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('styles/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<script src="{{asset('styles/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('styles/plugins/sparklines/sparkline.js')}}"></script>
<script src="{{asset('styles/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{asset('styles/plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>
<script src="{{asset('styles/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<script src="{{asset('styles/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('styles/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('styles/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<script src="{{asset('styles/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script src="{{asset('styles/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<script src="{{asset('styles/dist/js/adminlte.js')}}"></script>
</body>
</html>
