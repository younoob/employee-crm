<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Employee\MainController;

Route::get('/', [AuthController::class, 'index']);
Route::post('/signin', [AuthController::class, 'login']);
Route::get('/logout', [AuthController::class, 'logout']);

Auth::routes();

Route::group(['prefix' => '/employee'], function() {
    Route::get('/', [MainController::class, 'index']);
    Route::get('/details/{id}', [MainController::class, 'details']);
    Route::post('/search', [MainController::class, 'search']);
});
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
