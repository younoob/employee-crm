#intructions

    *run this command*

1.clone with https:
   git clone https://gitlab.com/younoob/employee-crm.git
  clone with ssh
   git clone git@gitlab.com:younoob/employee-crm.git

   please chose one of two option
2.run composer install or composer update
3.customize your a .env file
4.run php artisan migrate
4.generate your application key with run php artisan key:generate
5.add this code to function login in vendor/laravel/ui/auth-backend/Authenticates.php
    
    <!-- 
        $domain = Arr::first(explode('.', request()->getHost()));
        $company = Company::where('website', $domain)->first();

        if($domain == 'localhost' || !$company) {
            return redirect()->back()->with('error', 'check your domain');
        }

        $employee = Employee::WhereHas('getCompanyName', function ($query) use($domain){
            $query->where('website', $domain);
        })->where('email', $request->email)->first();

        if(!$employee) {
            $msg = 'email not registered in '.request()->getHost();
            return redirect()->back()->with('error', $msg);
        }
     -->
6.run php artisan serve 
7.enjoy and try it on your browser