<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Auth\LoginController as DefaultLoginController;

class AuthController extends DefaultLoginController
{

    protected $redirectTo = '/employee';    
    
    public function __construct()
    {
        $this->middleware('guest:employee')->except('logout');
    }

    public function index()
    {
        return view('login');
    }

    public function username()
    {
        return 'email';
    }    
    
    protected function guard()
    {
        return Auth::guard('employee');
    }
}
