<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Employee;
use Auth;
use DB;

class MainController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:employee');
    }

    public function index()
    {
        $company_id = Auth::user()->company_id;
        $data = Employee::where('company_id', $company_id)->paginate(10);
        return view('employee.index', compact('data'));
    }

    public function details($id) 
    {
        $employee = Employee::find($id);
        return view('employee.details', compact('employee'));
    }

}
