<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Employee extends Authenticatable
{
    use HasFactory;

    protected $table = 'employesses';

    protected $fillable = ['fisrt_name', 'last_name', 'company_id', 'email', 'created_by', 'updated_by'];

    protected $guarded = ['id']; 

    protected $hidden = [
     'password',
    ];   

    public function getAuthPassword()
    {
     return $this->password;
    }

    public function getCompanyName() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function getCreatedBy() {
        return $this->belongsTo('App\Models\User', 'created_by');
    }

    public function getUpdatedBy() {
        return $this->belongsTo('App\Models\User', 'updated_by');
    }
}
